import { clone, onlyUnique, sort } from '@/utilities/helpers';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: [
      {
        id: '01-ОБ/2022',
        number: '159-F',
        type: 'Pickup',
        creationDate: '2022-07-15T10:21:35.315271',
      },
      {
        id: '02-ОБ/2022',
        number: 'a-35',
        type: 'Delivery',
        creationDate: '2022-07-25T15:37:45.084291',
      },
      {
        id: '03-ОБ/2022',
        number: 'B-411',
        type: 'Pick-point',
        creationDate: '2022-08-01T09:11:26.264541',
      },
      {

        id: '04-ОБ/2022',
        number: '35-Ad',
        type: 'Pickup',
        creationDate: '2022-08-01T21:41:19.353652',
      },
      {
        id: '05-ОБ/2022',
        number: 'Ah-510',
        type: 'Pick-point',
        creationDate: '2022-08-12T18:01:51.526705',
      },
    ],
    cloneData: [],
    dropdownFilter: [],
    activeFilter: '',
    activeSort: '',
    inputValue: '',
    dropdownSort: [
      {
        value: 'asc',
        title: 'По возрастанию',
      },
      {
        value: 'desc',
        title: 'По убыванию',
      },
    ],
    toggle: 'list',
    typeTranslate: [
      {
        en: 'Pickup',
        ru: 'Самовывоз',
      },
      {
        en: 'Delivery',
        ru: 'Курьерская доставка',
      },
      {
        en: 'Pick-point',
        ru: 'Доставка в пункт выдачи',
      },
    ],
  },
  getters: {
    data: (state) => state.data,
    inputValue: (state) => state.inputValue,
    cloneData: (state) => {
      let data = clone(state.data);
      data = sort(data, state.activeSort);
      if (state.activeFilter) data = data.filter((i) => i.type === state.activeFilter);
      if (state.inputValue) {
        data = data.filter((i) => {
          const str = i.number.toLowerCase();
          const reg = state.inputValue.toLowerCase();
          if (str.indexOf(reg) >= 0) return true;
          return false;
        });
      }
      return data;
    },
    typeTranslate: (state) => state.typeTranslate,
    toggle: (state) => state.toggle,
    dropdownFilter: (state) => state.data.map((i) => i.type).filter(onlyUnique).map(
      (value) => {
        const typeItem = state.typeTranslate.find((i) => i.en === value);
        return {
          value,
          title: typeItem.ru,
        };
      },
    ),
    activeFilter: (state) => state.activeFilter,
    dropdownSort: (state) => state.dropdownSort,
    activeSort: (state) => state.activeSort,
  },
  actions: {
    setToggle({ commit }, position) {
      commit('toggle', position);
    },
    deleteItem({ commit }, id) {
      commit('deleteItem', id);
    },
  },
  mutations: {
    toggle(state, position) {
      state.toggle = position;
    },
    updateInputValue(state, inputValue) {
      state.inputValue = inputValue;
    },
    deleteItem(state, id) {
      state.data = state.data.filter((i) => i.id !== id);
    },
  },
});
