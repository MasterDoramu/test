import TableView from '@/views/Table/TableView.vue';
import Vue from 'vue';
import VueRouter from 'vue-router';
import ListView from '../views/List/ListView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'list',
    component: ListView,
  },
  {
    path: '/table',
    name: 'table',
    component: TableView,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
