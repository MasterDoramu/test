export const onlyUnique = (value, index, self) => self.indexOf(value) === index;
export const clone = (obj) => JSON.parse(JSON.stringify(obj));
export const dateFormat = (date) => {
  function addZero(item) {
    if (item < 10) return `0${item}`;
    return item;
  }
  const d = new Date(date);

  const dformat = `${[
    addZero(d.getDate()),
    addZero(d.getMonth() + 1),
    addZero(d.getFullYear()),
  ].join('.')} ${
    [
      addZero(d.getHours()),
      addZero(d.getMinutes()),
      addZero(d.getSeconds()),
    ].join(':')}`;
  return dformat;
};
export const sort = (data, type) => {
  function onlyNumber(item) {
    return item.replace(/[^0-9]/g, '');
  }
  return data.sort((a, b) => {
    if (type === 'desc') return onlyNumber(b.number) - onlyNumber(a.number);
    if (type === 'asc') return onlyNumber(a.number) - onlyNumber(b.number);
    return true;
  });
};
